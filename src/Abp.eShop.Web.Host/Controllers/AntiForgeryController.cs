using Microsoft.AspNetCore.Antiforgery;
using Abp.eShop.Controllers;

namespace Abp.eShop.Web.Host.Controllers
{
    public class AntiForgeryController : eShopControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}
