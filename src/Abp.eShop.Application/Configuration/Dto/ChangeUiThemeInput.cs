﻿using System.ComponentModel.DataAnnotations;

namespace Abp.eShop.Configuration.Dto
{
    public class ChangeUiThemeInput
    {
        [Required]
        [StringLength(32)]
        public string Theme { get; set; }
    }
}
