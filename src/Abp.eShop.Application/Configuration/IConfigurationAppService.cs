﻿using System.Threading.Tasks;
using Abp.eShop.Configuration.Dto;

namespace Abp.eShop.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
