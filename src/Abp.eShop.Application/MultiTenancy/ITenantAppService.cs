﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.eShop.MultiTenancy.Dto;

namespace Abp.eShop.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}
