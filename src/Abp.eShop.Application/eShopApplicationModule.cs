﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Abp.eShop.Authorization;

namespace Abp.eShop
{
    [DependsOn(
        typeof(eShopCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class eShopApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<eShopAuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(eShopApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(cfg =>
            {
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg.AddProfiles(thisAssembly);
            });
        }
    }
}
