﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.eShop.MultiTenancy;

namespace Abp.eShop.Sessions.Dto
{
    [AutoMapFrom(typeof(Tenant))]
    public class TenantLoginInfoDto : EntityDto
    {
        public string TenancyName { get; set; }

        public string Name { get; set; }
    }
}
