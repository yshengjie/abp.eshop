﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.eShop.Sessions.Dto;

namespace Abp.eShop.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
