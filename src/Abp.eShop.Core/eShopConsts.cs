﻿namespace Abp.eShop
{
    public class eShopConsts
    {
        public const string LocalizationSourceName = "eShop";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;
    }
}
