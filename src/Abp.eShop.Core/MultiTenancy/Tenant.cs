﻿using Abp.MultiTenancy;
using Abp.eShop.Authorization.Users;

namespace Abp.eShop.MultiTenancy
{
    public class Tenant : AbpTenant<User>
    {
        public Tenant()
        {            
        }

        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {
        }
    }
}
