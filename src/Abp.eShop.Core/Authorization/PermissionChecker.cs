﻿using Abp.Authorization;
using Abp.eShop.Authorization.Roles;
using Abp.eShop.Authorization.Users;

namespace Abp.eShop.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
