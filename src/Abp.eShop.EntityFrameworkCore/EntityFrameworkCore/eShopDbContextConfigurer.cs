using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace Abp.eShop.EntityFrameworkCore
{
    public static class eShopDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<eShopDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<eShopDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}
