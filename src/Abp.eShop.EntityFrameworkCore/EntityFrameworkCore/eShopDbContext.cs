﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using Abp.eShop.Authorization.Roles;
using Abp.eShop.Authorization.Users;
using Abp.eShop.MultiTenancy;

namespace Abp.eShop.EntityFrameworkCore
{
    public class eShopDbContext : AbpZeroDbContext<Tenant, Role, User, eShopDbContext>
    {
        /* Define an IDbSet for each entity of the application */
        
        public eShopDbContext(DbContextOptions<eShopDbContext> options)
            : base(options)
        {
        }
    }
}
