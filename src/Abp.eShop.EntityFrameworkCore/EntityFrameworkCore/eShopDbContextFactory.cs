﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Abp.eShop.Configuration;
using Abp.eShop.Web;

namespace Abp.eShop.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class eShopDbContextFactory : IDesignTimeDbContextFactory<eShopDbContext>
    {
        public eShopDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<eShopDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            eShopDbContextConfigurer.Configure(builder, configuration.GetConnectionString(eShopConsts.ConnectionStringName));

            return new eShopDbContext(builder.Options);
        }
    }
}
