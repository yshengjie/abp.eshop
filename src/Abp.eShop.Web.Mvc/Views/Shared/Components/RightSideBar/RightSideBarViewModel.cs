﻿using Abp.eShop.Configuration.Ui;

namespace Abp.eShop.Web.Views.Shared.Components.RightSideBar
{
    public class RightSideBarViewModel
    {
        public UiThemeInfo CurrentTheme { get; set; }
    }
}
