﻿using Abp.AspNetCore.Mvc.ViewComponents;

namespace Abp.eShop.Web.Views
{
    public abstract class eShopViewComponent : AbpViewComponent
    {
        protected eShopViewComponent()
        {
            LocalizationSourceName = eShopConsts.LocalizationSourceName;
        }
    }
}