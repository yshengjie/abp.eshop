﻿using Abp.AspNetCore.Mvc.Views;
using Abp.Runtime.Session;
using Microsoft.AspNetCore.Mvc.Razor.Internal;

namespace Abp.eShop.Web.Views
{
    public abstract class eShopRazorPage<TModel> : AbpRazorPage<TModel>
    {
        [RazorInject]
        public IAbpSession AbpSession { get; set; }

        protected eShopRazorPage()
        {
            LocalizationSourceName = eShopConsts.LocalizationSourceName;
        }
    }
}
