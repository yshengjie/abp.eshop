﻿using Abp.Modules;
using Abp.Reflection.Extensions;
using Abp.eShop.Configuration;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace Abp.eShop.Web.Startup
{
    [DependsOn(typeof(eShopWebCoreModule))]
    public class eShopWebMvcModule : AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public eShopWebMvcModule(IHostingEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void PreInitialize()
        {
            Configuration.Navigation.Providers.Add<eShopNavigationProvider>();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(eShopWebMvcModule).GetAssembly());
        }
    }
}