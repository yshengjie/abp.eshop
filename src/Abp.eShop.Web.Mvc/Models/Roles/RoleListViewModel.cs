﻿using System.Collections.Generic;
using Abp.eShop.Roles.Dto;

namespace Abp.eShop.Web.Models.Roles
{
    public class RoleListViewModel
    {
        public IReadOnlyList<RoleDto> Roles { get; set; }

        public IReadOnlyList<PermissionDto> Permissions { get; set; }
    }
}
