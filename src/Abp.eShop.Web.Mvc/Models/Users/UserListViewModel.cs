using System.Collections.Generic;
using Abp.eShop.Roles.Dto;
using Abp.eShop.Users.Dto;

namespace Abp.eShop.Web.Models.Users
{
    public class UserListViewModel
    {
        public IReadOnlyList<UserDto> Users { get; set; }

        public IReadOnlyList<RoleDto> Roles { get; set; }
    }
}