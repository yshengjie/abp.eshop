﻿using Abp.AspNetCore.Mvc.Authorization;
using Abp.eShop.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace Abp.eShop.Web.Controllers
{
    [AbpMvcAuthorize]
    public class HomeController : eShopControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
	}
}