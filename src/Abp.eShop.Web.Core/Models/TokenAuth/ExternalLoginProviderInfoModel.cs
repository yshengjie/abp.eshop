﻿using Abp.AutoMapper;
using Abp.eShop.Authentication.External;

namespace Abp.eShop.Models.TokenAuth
{
    [AutoMapFrom(typeof(ExternalLoginProviderInfo))]
    public class ExternalLoginProviderInfoModel
    {
        public string Name { get; set; }

        public string ClientId { get; set; }
    }
}
